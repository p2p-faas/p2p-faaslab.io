---
layout: page
title: Getting Started
permalink: /getting-started
---

We suggest to deploy the framework with [OpenBalena](https://www.balena.io/open/) which allows to install docker containers within a set of nodes, like Raspberry Pi. However, you are free to chose your personal way of deploying the framework to all the nodes. The `docker-compose.yml` needed for booting the framework is within the [`stack`](https://gitlab.com/p2p-faas/stack). 

# Booting

For starting the bare framework, you can use the command

```
docker-compose up
```

## With FaaS function

But for running a benchmark you need to install a FaaS function. In [[1]](https://dx.doi.org/10.1109/PerComWorkshops53856.2022.9767498) and [[2]](https://dx.doi.org/10.1109/TCC.2020.2968443) the `pigo-openfaas` [[3]](https://github.com/esimov/pigo-openfaas) function is used. You can clone it in this folder and then:

1. Build it with `faas-cli` [[4]](https://github.com/openfaas/faas-cli), this will create the Dockerfile needed for starting the Compose

    ```
    faas-cli build
    ```

2. Start the framwork

    ```
    docker-compose up -f docker-compose-fn.yml
    ```

## In SBCs (Single Board Computers e.g. Raspberry Pi)

For booting the framework in a set of SBCs nodes, starting from this repository and after cloning and building the functions like described above, you can use the OpenBalena [[5]](https://www.balena.io/open/) framework. Once all is set up, use the command to deploy the framework by building it within a node

```
balena deploy admin/myfleet -m -h <node-ip> -p 2375 --debug --build
```
