---
layout: page
title: Home
permalink: /
---

Welcome to the home page of the **P2PFasS** framework. Follow the [Getting Started](/getting-started) section for knowing how to install the framwork.

P2PFaaS allows the implementation of custom distributed scheduling and load balancing algorithms in a set of Edge or Fog nodes which are able to communicate with each other.

The algorithms already implemented are **cooperative** and obviously **peer-to-peer** in the sense that nodes decide to forward part of their traffic to the neighbours. The atomic task entity that is forwarded and processed is a **FaaS** invocation via REST API. 

For having a first glance about the architecture of the framework you can read the paper describing the framework at [https://doi.org/10.1016/j.softx.2022.101290](https://doi.org/10.1016/j.softx.2022.101290).

If you use your framework in you research please cite

```bibtex
@article{PROIETTIMATTIA2023101290,
title = {P2PFaaS: A framework for FaaS peer-to-peer scheduling and load balancing in Fog and Edge computing},
journal = {SoftwareX},
volume = {21},
pages = {101290},
year = {2023},
issn = {2352-7110},
doi = {https://doi.org/10.1016/j.softx.2022.101290},
url = {https://www.sciencedirect.com/science/article/pii/S2352711022002084},
author = {Gabriele {Proietti Mattia} and Roberto Beraldi},
keywords = {Edge Computing, Fog Computing, FaaS},
abstract = {In Edge and Fog Computing environments, it is usual to design and test distributed algorithms that implement scheduling and load balancing solutions. The operation paradigm that usually fits the context requires the users to make calls to the closer node for executing a task, and since the service must be distributed among a set of nodes, the serverless paradigm with the FaaS (Function-as-a-Service) is the most promising strategy to use. In light of these preconditions, we designed and implemented a framework called P2PFaaS. The framework, built upon Docker containers, allows the implementation of fully decentralised scheduling or load balancing algorithms among a set of nodes. By relying on three basic services, such as the scheduling service, the discovery service, and the learner service, the framework allows the implementation of any kind of scheduling solution, even if based on Reinforcement Learning. Finally, the framework provides a ready-to-go solution that can be installed and has been tested both on x86 servers and ARM-based edge nodes (like, for example, the Raspberry Pi).}
}
```

or 


```bibtex
@article{8964273,
    author={Beraldi, Roberto and Proietti Mattia, Gabriele},
    journal={IEEE Transactions on Cloud Computing},
    title={Power of random choices made efficient for fog computing},
    year={2020},
    volume={},
    number={},
    pages={1-1},
    doi={10.1109/TCC.2020.2968443}
}
```

Thank you!