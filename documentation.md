---
layout: page
title: Docs
permalink: /documentation
---

The documentation is available for all the modules that compose the framework:

- the [scheduler](/docs/stack-scheduler/pkg)
- the [discovery](/docs/stack-discovery/pkg)
- the [learner](/docs/stack-learner/)

Follow each link to see the docs of each module.