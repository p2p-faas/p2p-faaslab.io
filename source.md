# Source

The P2PFaaS framework is fully modular and the source code is split among multiple repositories, available at [https://gitlab.com/p2p-faas](https://gitlab.com/p2p-faas).